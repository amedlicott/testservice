package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TestService.TestServiceServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TestServiceService.TestServiceOperationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TestServiceService.TestServiceOperationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class TestServiceOperationTests {

	@Test
	public void testOperationTestServiceOperationBasicMapping()  {
		TestServiceServiceDefaultImpl serviceDefaultImpl = new TestServiceServiceDefaultImpl();
		TestServiceOperationInputParametersDTO inputs = new TestServiceOperationInputParametersDTO();
		inputs.setRequestModel(org.apache.commons.lang3.StringUtils.EMPTY);
		TestServiceOperationReturnDTO returnValue = serviceDefaultImpl.testServiceOperation(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}